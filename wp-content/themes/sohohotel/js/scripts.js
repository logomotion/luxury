// Custom

jQuery('.menu-item a').click(function(e){
	e.preventDefault();
	href = jQuery(this).attr('href');
	if (href !== '#') {
		window.location.href = href;
	}
});

// Set Variables
var mobile_toggle = 'closed';

jQuery(document).ready(function() { 
	
	"use strict";
	
	// Main Menu Drop Down
	jQuery('ul#navigation').superfish({ 
        delay:       600,
        animation:   {opacity:'show',height:'show'},
        speed:       'fast',
        autoArrows:  true,
        dropShadows: false
    });

	// Language Drop Down
	jQuery('ul#language-selection').superfish({ 
        delay:       600,
        animation:   {opacity:'show',height:'show'},
        speed:       'fast',
        autoArrows:  true,
        dropShadows: false
    });
	
	// Accordion
	jQuery( ".accordion" ).accordion( { heightStyle: "content" } );

	// Toggle	
	jQuery( ".toggle > .inner" ).hide();
	jQuery(".toggle .title").bind('click',function() {
		jQuery(this).toggleClass('active');
		if (jQuery(this).hasClass('active')) {
			jQuery(this).closest('.toggle').find('.inner').slideDown(200, 'easeOutCirc');
		} else {
			jQuery(this).closest('.toggle').find('.inner').slideUp(200, 'easeOutCirc');
		}
	});
	
	// Tabs
	jQuery(function() {
		jQuery( ".tabs" ).tabs({
			activate: function (event, ui) {
				if ( ui.newPanel.data("map") ){
					var mapContent = ui.newPanel.data("map") ;
					google.maps.event.trigger(mapContent, 'resize');
					mapContent.setCenter(mapContent.Center);
				}
			}
		});
		 
	});
	
	// PrettyPhoto
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false});
	
	// Search Button Toggle
	jQuery(".menu-search-button").click(function() {
		jQuery(".menu-search-field").toggleClass("menu-search-focus", 200);
	});
	
	// Mobile Menu
	jQuery(".mobile-menu-button, .mobile-menu-title").click(function(){
		jQuery(".mobile-menu-inner").stop().slideToggle(350);
		return false;
	});
	
	// Header Google Map by .gmap-button
	jQuery(".gmap-button").click(function(e){
		e.preventDefault();
		jQuery('#header-gmap').toggleClass('open');
		jQuery('.gmap-button').toggleClass('gmap-button-hover');
	});
	
	// Header Google Map by #toggle-maps
	jQuery("#toggle-maps").click(function(e){
		e.preventDefault();
		jQuery('html, body').animate({ scrollTop: 0 }, 500);
		setTimeout(function(){
			jQuery('#header-gmap').toggleClass('open');
			jQuery('.gmap-button').toggleClass('gmap-button-hover');
		}, 500);
	});
	
	// Init from the start
	initialize();

});

jQuery(window).load(function(){
	
	"use strict";
	
	jQuery('.accommodation-slider li').each(function(){
		var src = jQuery(this).find('img').attr('src');
		jQuery(this).attr('data-thumb', src);
	});

	// Main Slider
	jQuery('.slider, .slideshow-shortcode').flexslider({
		animation: "fade",
		controlNav: "thumbnails",
		directionNav: true,
		slideshow: slideshow_autoplay,
		start: function(slider){
			jQuery('body').removeClass('loading');
		},
		prevText: "",
		nextText: "",
		smoothHeight: true
	});
	
	// Text Slider
	jQuery('.text-slider').flexslider({
		animation: "fade",
		controlNav: false,
		directionNav: true,
		slideshow: true,
		start: function(slider){
			jQuery('body').removeClass('loading');
		},
		prevText: "",
		nextText: ""
	});

	jQuery('.accommodation-slider').css('padding-bottom', jQuery('.flex-control-thumbs').height());

});