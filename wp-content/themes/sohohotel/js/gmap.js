/*var map = null;

var gmap_styles = [];
if (gmap_options.hide_businesses == 1) {
	gmap_styles = [{ featureType: 'poi.business', stylers: [{ visibility: 'off' }] }];
}

function initialize(Lat,Lng) {
	var latlng = new google.maps.LatLng(47.3162159, 5.0424962);
	var myOptions = {
		zoom: 15,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		},
		styles: gmap_styles
	};

	map = new google.maps.Map(document.getElementById("map-canvas"),myOptions);
	var contentString = googlemapMarker;
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});

	var marker = new google.maps.Marker({
		position: latlng, 
		map: map
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});

}*/

// GMaps

var map ;
var openInfowindow = null ;
var newPoints = [] ;
var zoneMarqueurs = null ;

function initialize (Lat,Lng) {
	
	var mapOptions = {
		center: new google.maps.LatLng(47.3162159, 5.0424962),
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		streetViewControl: false
	};
	
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	addPoints();
	map.fitBounds(zoneMarqueurs);
	
}

function addPoints () {
	
	isEN = window.location.href.indexOf('/en/') > -1 ? true : false;

	if (isEN) {
		newPoints[0] = ['47.3211964', '5.039613799999984', ['Loft 27'], ['http://www.luxuryflatindijon.fr/en/accommodation/loft27-2/']];
		newPoints[1] = ['47.3194708', '5.041708299999982', ['Amiral Roussin'], ['http://www.luxuryflatindijon.fr/en/accommodation/amiral-roussin-2/']];
		newPoints[2] = ['47.3206596', '5.041838200000029', ['Bénigne le Compasseur', 'Logis des Fremyot', 'Baronne de Chantal'], ['http://www.luxuryflatindijon.fr/en/accommodation/benigne-le-compasseur-2/', 'http://www.luxuryflatindijon.fr/en/accommodation/logis-des-fremyot-2/', 'http://www.luxuryflatindijon.fr/en/accommodation/baronne-de-chantal-2/']];
		newPoints[3] = ['47.3197150', '5.039920199999983', ['Grand Amiral'], ['http://www.luxuryflatindijon.fr/en/accommodation/grand-amiral-2/']];
	} else {
		newPoints[0] = ['47.3211964', '5.039613799999984', ['Loft 27'], ['http://www.luxuryflatindijon.fr/accommodation/loft27/']];
		newPoints[1] = ['47.3194708', '5.041708299999982', ['Amiral Roussin'], ['http://www.luxuryflatindijon.fr/accommodation/amiral-roussin/']];
		newPoints[2] = ['47.3206596', '5.041838200000029', ['Bénigne le Compasseur', 'Logis des Fremyot', 'Baronne de Chantal'], ['http://www.luxuryflatindijon.fr/accommodation/benigne-le-compasseur/', 'http://www.luxuryflatindijon.fr/accommodation/logis-des-fremyot/', 'http://www.luxuryflatindijon.fr/accommodation/baronne-de-chantal/']];
		newPoints[3] = ['47.3197150', '5.039920199999983', ['Grand Amiral'], ['http://www.luxuryflatindijon.fr/accommodation/grand-amiral/']];
	}
	
	zoneMarqueurs = new google.maps.LatLngBounds();
	
	for (var i = 0; i < newPoints.length; i++) {

		var position = new google.maps.LatLng(newPoints[i][0], newPoints[i][1]);
		var marker = new google.maps.Marker({
			position: position,
			map: map,
			animation: google.maps.Animation.DROP
		});

		createMarker(marker, i);
		zoneMarqueurs.extend(marker.getPosition());

	}
	
}

function createMarker (marker, i) {

	var contentOfPopup = '<div class="popup"><p>';

	for (var y = 0; y < newPoints[i][3].length; y++) {
		contentOfPopup += '<a href="' + newPoints[i][3][y] + '">' + newPoints[i][2][y] + '</a>';
	}

	contentOfPopup += '</p></div>';

	var infowindow = new google.maps.InfoWindow({
		content: contentOfPopup
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});

}